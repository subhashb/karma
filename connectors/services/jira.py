from jira import JIRA
import sys
from user.models import Account
from rules.models import Rule, Point
from django.core.mail import send_mail
from datetime import datetime, timedelta


class JiraService():
    def __init__(self, juser, jpass, jserver):
        options = {
            'server': jserver
        }

        try:
            self.jira = JIRA(options, basic_auth=(juser, jpass))

            allfields = self.jira.fields()
            self.nameMap = {field['name']: field['id'] for field in allfields}
        except:
            print("Could not connect to Jira: %s", sys.exc_info())
            sys.exit()

    def jira_001(self):
        rule = Rule.objects.get(identifier="JIRA-001")

        overdue_epics = self.jira.search_issues(
            "type = Epic and "
            "resolution = Unresolved and "
            "project = 'Engineering Projects'  and "
            "'Expected in Production' < startOfDay() and "
            "'Expected in Production' > -5d "
            "order by Assignee")

        account_epics = {}
        # Loop thorugh all overdue epics
        for epic in overdue_epics:
            # Check if assignee in epic is present as user
            try:
                account = Account.objects.get(
                    jira_username=epic.fields.assignee.name)

                # Group Epics by user
                if account.jira_username in account_epics.keys():
                    account_epics[account.jira_username][1].append(epic)
                else:
                    account_epics[account.jira_username] = (account, [epic])
            except Account.DoesNotExist:
                pass

        # Send details to user's email
        for key, value in account_epics.items():
            # value[0] is user
            # value[1] is list of jira issues
            send_mail(
                'Subject here',
                ' '.join([issue.key for issue in value[1]]),
                'kaybusbot@gmail.com',
                [value[0].email],
                fail_silently=False)

            for epic in value[1]:
                due_date = datetime.strptime(
                            getattr(epic.fields,
                                    self.nameMap["Expected in Production"]),
                            '%Y-%m-%d').date()

                if due_date <= datetime.now().date() - timedelta(days=3):
                    message = "{0} is overdue for more than 3 days" \
                              .format(epic.key)

                    # Apply negative points beyond 3 days of due date
                    # Deduct just once per epic
                    try:
                        Point.objects.get(
                            account=value[0],
                            rule=rule,
                            message=message)
                    except Point.DoesNotExist:
                        Point.objects.create(
                            account=value[0],
                            rule=rule,
                            points=rule.points,
                            message=message)
