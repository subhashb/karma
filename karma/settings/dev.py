from .common import * # NOQA

# DEBUG CONFIGURATION
DEBUG = True
# END DEBUG CONFIGURATION

# Email Configuration
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = 'tmp/emails'
