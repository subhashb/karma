from django.contrib import admin
from rules.models import Rule


class RuleAdmin(admin.ModelAdmin):
    list_display = ('identifier', 'name', 'is_enabled')

admin.site.register(Rule, RuleAdmin)
