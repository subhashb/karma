from django.db import models
from karma.models import BaseModel
from user.models import Account


class Rule(BaseModel):
    class Meta:
        verbose_name_plural = "rules"

    name = models.CharField(verbose_name='Rule Name',
                            max_length=35)
    description = models.CharField(verbose_name='Rule Description',
                                   max_length=255)
    identifier = models.CharField(verbose_name="Unique Rule Identifier",
                                  max_length=15)
    points = models.SmallIntegerField()
    is_enabled = models.BooleanField(
        verbose_name='Is Rule Enabled',
        default=False,
        help_text='Designates whether the rule has been activated and usable',
    )


class Point(BaseModel):
    class Meta:
        verbose_name_plural = "points"

    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    rule = models.ForeignKey(Rule, on_delete=models.CASCADE)
    points = models.SmallIntegerField()
    message = models.CharField(verbose_name='Details of point allocation',
                               max_length=255)
