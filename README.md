## VirtualEnv Setup
    mkdir ~/.venvs
    cd ~/.venvs
    python3 -m venv karmadev
    source ~/.venvs/karmadev/bin/activate

## Installing Pre-requisites
    pip install -r requirements.txt

## Postgres Commands to set up Database
    CREATE USER karmadev WITH PASSWORD 'karmadev';
    CREATE DATABASE karmadev;
    ALTER ROLE karmadev SET client_encoding TO 'utf8';
    ALTER ROLE karmadev SET default_transaction_isolation TO 'read committed';
    ALTER ROLE karmadev SET timezone TO 'UTC';
    GRANT ALL PRIVILEGES ON DATABASE karmadev to karmadev;
    \c karmadev
    CREATE SCHEMA app AUTHORIZATION karmadev;

## Running Tests
    ./manage.py test

## Generating Coverage
    coverage run ./manage.py test
    coverage report

## Curl Examples using [httpie](https://github.com/jkbrzt/httpie)
	 http -f post http://localhost:8000/api-token-auth/ email=john.doe@example.com password=random (Retrieve JWT Token)

## Coding Standards
    pip install flake8
    apm install linter-flake8

## Dumping and loading data
    python manage.py dumpdata rules.Rule --indent 2 > rules/fixtures/rules.json
    python manage.py loaddata rules.json
