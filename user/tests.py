from user.models import Account
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from rest_framework_jwt import utils
from unittest import skip


@skip("Login tests")
class LoginAsSuperuserTest(APITestCase):
    def setUp(self):
        self.csrf_client = APIClient(enforce_csrf_checks=True)

    def test_invalid_login_with_no_auth_header(self):
        """
        Ensure Anonymous requests cannot be made to protected APIs
        """
        pass


class CreateAccountAsSuperuserTest(APITestCase):
    def setUp(self):
        self.csrf_client = APIClient(enforce_csrf_checks=True)
        self.email = 'jpueblo@example.com'
        self.password = 'password123'
        self.first_name = 'jon'
        self.last_name = 'pueblo'
        self.user = Account.objects.create_superuser(self.email,
                                                     self.password,
                                                     self.first_name,
                                                     self.last_name)

    def test_create_account_by_superuser(self):
        """
        Ensure SuperUsers can create accounts
        """
        payload = utils.jwt_payload_handler(self.user)
        token = utils.jwt_encode_handler(payload)

        auth = 'JWT {0}'.format(token)
        response = self.csrf_client.post(
            '/accounts/',
            {'email': 'subhashb@example.com',
             'password': 'password123',
             'first_name': 'subhashb',
             'last_name': 'user'},
            HTTP_AUTHORIZATION=auth,
            format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class CreateAccountAsAnonymousTest(APITestCase):
    def setUp(self):
        self.csrf_client = APIClient(enforce_csrf_checks=True)

    def test_create_account_by_anonymous(self):
        """
        Ensure Anonymous users can register themselves
        """
        response = self.csrf_client.post(
            '/accounts/',
            {'email': 'subhashb@example.com',
             'password': 'password123',
             'first_name': 'subhashb',
             'last_name': 'user'},
            format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class CreateAccountAsNormalUserTest(APITestCase):
    def setUp(self):
        self.csrf_client = APIClient(enforce_csrf_checks=True)
        self.email = 'jpueblo@example.com'
        self.password = 'password123'
        self.first_name = 'jon'
        self.last_name = 'pueblo'
        self.user = Account.objects.create_user(self.email,
                                                self.password,
                                                self.first_name,
                                                self.last_name)

    def test_create_account_by_normal_user(self):
        """
        Ensure Normal Users cannot create accounts
        """
        self.user.is_staff = False
        self.user.save

        payload = utils.jwt_payload_handler(self.user)
        token = utils.jwt_encode_handler(payload)

        auth = 'JWT {0}'.format(token)
        response = self.csrf_client.post(
            '/accounts/',
            {'email': 'subhashb@example.com',
             'password': 'password123',
             'first_name': 'subhashb',
             'last_name': 'user'},
            HTTP_AUTHORIZATION=auth,
            format='json')

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
