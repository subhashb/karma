from rest_framework import permissions


class IsOwnerOrCreate(permissions.BasePermission):
    """
    Custom permission to restrict API to admins,
    except CREATE for anon users to register themselves
    """

    def has_permission(self, request, view):
        return (request.user.is_staff or
                (request.method == "POST" and
                 (request.user.is_staff or
                  not request.user.is_authenticated())))
