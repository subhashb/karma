from django.db import models
from django.utils import timezone
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from karma.models import BaseModel


class AccountManager(BaseUserManager):
    def create_user(self, email, password, first_name, last_name):
        """
        Creates and saves a User with the given email, first_name,
        last_name and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, first_name, last_name):
        """
        Creates and saves a SuperUser with the given email, first_name,
        last_name and password.
        """

        if not email:
            raise ValueError('Users must have an email address')

        user = self.create_user(email,
                                password=password,
                                first_name=first_name,
                                last_name=last_name,
                                )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser, PermissionsMixin, BaseModel):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Required fields: Email, Password
    """
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    first_name = models.CharField(verbose_name='first name',
                                  max_length=35, blank=True)
    last_name = models.CharField(verbose_name='last name',
                                 max_length=35, blank=True)
    # FIXME Add Gender, Language, Picture
    is_staff = models.BooleanField(
        verbose_name='staff status',
        default=False,
        help_text='Designates whether the user can log into this admin site.',
    )
    is_active = models.BooleanField(
        verbose_name='active',
        default=True,
        help_text='Designates whether this user should be treated as active. '
                  'Unselect this instead of deleting accounts.',
    )
    joined_at = models.DateTimeField(verbose_name='date joined',
                                     default=timezone.now)
    github_username = models.CharField(verbose_name="Github Username",
                                       max_length=35, blank=True)
    jira_username = models.CharField(verbose_name="Jira Username",
                                     max_length=35, blank=True)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def __str__(self):
        return self.email
